1)Are all functions commented?

Not all the functions have comments that describe their function. For example functions such as void repaint() on line 493 have no comment at all above the declaration. This results in the no immediate way for somebody to understand the functionality and uses for the methods. Some functions do have minimal comments above the declaration such as void updateGameState on line 573. However this comment is not particularly useful as it is simply just saying the function name again. There is no additional information conveyed and commenting for the sake of commenting is not helpful.

2) Is there any incomplete code? If so, should it be removed or flagged with a suitable marker like ‘TODO’?

There are no incomplete code. However this is not immediately obvious as if one does a search for 'TODO' they will find 1 match (lines 351). The comment says "TODO: collison", yet this has already been implemented. What I should have done is to remember to remove this comment after I implemented it. Since I did not, another person will believe that collison needs to be implemented and perhaps waste time only to later find out it has already been coded. 

3) Is there any commented out code?
There is indeed commented out code. Notably on line 358 exists a commented debugging code that was used during coding. This line is not needed however yet it was not removed. This is not too destructive however as it is only one line of code. However in the future this line of code should be removed, or in the case that it might be useful for future persons working on it, the code should be commented to note its function and why it is commented out but still included.