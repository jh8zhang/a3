#include <iostream>
#include <list>
#include <cstdlib>
#include <sys/time.h>
#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <string>
/*
 * Header files for X functions
 */
#include <X11/Xlib.h>
#include <X11/Xutil.h>

using namespace std;

#include <sstream>

namespace patch {
  template < typename T > std::string to_string( const T& n )
  {
      std::ostringstream stm ;
      stm << n ;
      return stm.str() ;
  }
}

// random number
int randomInt (int lower, int upper){
	int range = upper - lower + 1;

	int r = rand() % range + lower; 
	return r;
}

// implement later lol
void updateGameState (int);
 
/*
 * Global game state variables
 */
const int Border = 1;
const int BufferSize = 10;
const int width = 800;
const int height = 600;

const int GAME_AREA_X = 50;
const int GAME_AREA_Y = 100;
const int BLOCKSIZE = 25;
const int AREA_WIDTH_IN_BLOCKS = 28;
const int AREA_HEIGHT_IN_BLOCKS = 18;

// FPS taken by arg
int FPS = 30;
int score = 0;
int speed = 0;

// game states
// 0 - splash screen
// 1 - game running
// 2 - paused
// 3 - game over
int gameState = 0;

unsigned fruit_w, fruit_h;

Pixmap bmp_fruit;

/*
 * Information to draw on the window.
 */
struct XInfo {
	Display	 *display;
	int		 screen;
	Window	 window;
	GC		 gc[3];
	int		width;		// size of window
	int		height;
};

/*
 * Function to put out a message on error exits.
 */
void error( string str ) {
  cerr << str << endl;
  exit(0);
}

/*
 * An abstract class representing displayable things. 
 */
class Displayable {
	public:
		virtual void paint(XInfo &xinfo) = 0;
};

// game area 
class GameArea : public Displayable {
	public:
		virtual void paint (XInfo& xinfo){
			XDrawRectangle (xinfo.display, xinfo.window, xinfo.gc[0], x, y, width, height);
		};

		GameArea (int x, int y, int width, int height): x(x), y(y), width(width), height(height) {}
		
		// returns whether an (x, y) is in the game area
		bool isInArea (int _x, int _y) {
			return (_x >= x && _x <= x + width - BLOCKSIZE && _y >= y && _y <= y + height - BLOCKSIZE);
		}

	private:
		int x, y, width, height;
};

class Body : public Displayable {
	public:

		Body (int x, int y, Body* prev): x(x), y(y), prev(prev){
			next = NULL;
		}

		virtual void paint(XInfo &xinfo) {
			if (next != NULL){
				next->paint(xinfo);
			}
			XFillRectangle(xinfo.display, xinfo.window, xinfo.gc[0], x, y, BLOCKSIZE, BLOCKSIZE);
		}

		void newPosition(int _x, int _y){
			if (NULL != next){
				next-> newPosition(x, y);
			}

			x_o = x;
			y_o = y;

			x = _x;
			y = _y;
		}

		void grow (int _x, int _y) {
			if (next != NULL){
				next-> grow(x_o, y_o);
			} else {
				next = new Body(x_o, y_o, this);
			}
		}

		bool collision(int _x, int _y){
			if (_x == x && _y == y){
				return true;
			}

			if (next == NULL){
				return false;
			} else {
				return next->collision(_x, _y);
			}
		}

		// checks if the snake occupies this xy coordinate
    bool occupyXandY (int cor_x, int cor_y){
    	if (next == NULL){
    		return (cor_x == x && cor_y == y);
    	} else {
    		return (cor_x == x && cor_y == y) || next-> occupyXandY(cor_x, cor_y);
    	}
    }

		~Body (){
			if (NULL != next){
				delete(next);
			}
		}
	private:
		int x,y, x_o, y_o;
		Body *prev, *next;
};

class Snake : public Displayable {
	public:
		virtual void paint(XInfo &xinfo) {
			XFillRectangle(xinfo.display, xinfo.window, xinfo.gc[0], x, y, BLOCKSIZE, BLOCKSIZE);
			_changedDirection = false;
			// also draw the body
			if (body != NULL){
				body-> paint(xinfo);
			}

			checkAteSelf();
		}
		
		// return whether the snake moves on the screen
		bool move(XInfo &xinfo) {
			x_o = x;
			y_o = y;

			// make speed independent of FPS and multiple of the block size
			_x += (speed*1000/(double)FPS) * (double)direction_X;
			_y += (speed*1000/(double)FPS) * (double)direction_Y;
			
			// only redraw when _x is changed by multiple of block size (ensures alignment)
			x = (int)_x / BLOCKSIZE;
			x -= x % 25;
			y = (int)_y / BLOCKSIZE;
			y -= y % 25;

			if (x != x_o || y != y_o){
				// update new position of body 
				if (body != NULL){
					body->newPosition(x_o, y_o);
				}
				return true;
			}

			return false;
		}

		void setSpeed(int new_speed) {
			speed = new_speed;
		}
		
		int getX() {
			return x;
		}
		
		int getY() {
			return y;
		}

		// grows the snake
    void grow(XInfo &xinfo) {
    	if (NULL == body){
    		body = new Body(x_o, y_o, NULL);
    	} else {
    		body->grow(x_o, y_o);
    	}

    	body->paint(xinfo);
    }

    void checkAteSelf() {
    	if (body == NULL){
    		_ateSelf = (_ateSelf || false);
    	} else {
    		_ateSelf = (_ateSelf || body->collision(x, y));
    	}
    }

    bool ateSelf(){
    	return _ateSelf;
    }

    // checks if the snake occupies this xy coordinate
    bool occupyXandY (int cor_x, int cor_y){
    	if (body == NULL){
    		return (cor_x == x && cor_y == y);
    	} else {
    		return (cor_x == x && cor_y == y) || body-> occupyXandY(cor_x, cor_y);
    	}
    }

    // change direction based on user input
    // can only change once per move
    void onUserChangeDirection(char key){
    	if (_changedDirection ){
    		return;
    	}
    	// each direction change can be triggered
    	// UNLESS snake is going on the axis
    	switch (key){
    		case 'w':
    			if (direction_Y == 0){
    				direction_Y = -1;
    				direction_X = 0;
    				_changedDirection = true;
    			}
    			break;
    		case 'a':
    			if (direction_X == 0){
    				direction_X = -1;
    				direction_Y = 0;
    				_changedDirection = true;
    			}
    			break;
    		case 's':
    			if (direction_Y == 0){
    				direction_Y = 1;
    				direction_X = 0;
    				_changedDirection = true;
    			}
    			break;
    		case 'd':
    			if (direction_X == 0){
    				direction_X = 1;
    				direction_Y = 0;
    				_changedDirection = true;
    			}
    			break;
    	}
    }
		
		Snake(int x, int y): x(x), y(y) {
			direction_X = 0;
			direction_Y = 0;
     	speed = 0; // init to 0
     	_x = x * BLOCKSIZE;
     	_y = y * BLOCKSIZE;

     	// ensures snake is initially placed on a proper position
     	x = _x / BLOCKSIZE;
     	y = _y / BLOCKSIZE;

     	body = NULL;

     	_changedDirection = false;
     	_ateSelf = false;
		}

		~Snake (){
			delete(body);
		}
	
	private:
		Body* body;

		// some flags
		bool _ateSelf, _changedDirection; // need to buffer

		int x, x_o;
		int y, y_o;
		double _x, _y; 
		int direction_X;
		int direction_Y;
		int speed; // 1 - 10 speed
};

class Fruit : public Displayable {
	public:
		virtual void paint(XInfo &xinfo) {
			//XFillRectangle(xinfo.display, xinfo.window, xinfo.gc[0], x, y, BLOCKSIZE, BLOCKSIZE);
			XCopyPlane(xinfo.display, bmp_fruit, xinfo.window, xinfo.gc[0], 0, 0, fruit_w, fruit_h, x, y, 1);
    }

    Fruit() {
   		x = GAME_AREA_X + 7 * BLOCKSIZE;
   		y = GAME_AREA_Y + 3 * BLOCKSIZE;
    }

    void newPosition (Snake* snake){
    	// TODO : collison
    	int rand_w = randomInt(0, AREA_WIDTH_IN_BLOCKS - 1);
    	int rand_h = randomInt(0, AREA_HEIGHT_IN_BLOCKS - 1);
    
    	x = rand_w * BLOCKSIZE + GAME_AREA_X;
    	y = rand_h * BLOCKSIZE + GAME_AREA_Y; 
    	
    	// cerr << "new (x,y) = (" << x << ", " << y << ")" << endl;

    	if (snake->occupyXandY(x, y)){
    		return newPosition(snake); // gotta do it again
    	}
    }

    int getX () { return x; }
    int getY () { return y; }

	private:
    int x;
    int y;
};

// class that displays text
class Text : public Displayable {
	public:
	  virtual void paint(XInfo& xinfo) {
	      XDrawImageString( xinfo.display, xinfo.window, xinfo.gc[0],
	                        this->x, this->y, this->s.c_str(), this->s.length() );
	  }

	  void updateText (string newString){
	  	s = newString;
	  }

	  // constructor
	  Text(int x, int y, string s):x(x), y(y), s(s)  {}

	private:
	  //XPoint p; // a 2D point (see also http://tronche.com/gui/x/xlib/graphics/drawing/)
	  int x;
	  int y;
	  string s; // string to show
};

list<Displayable *> dList;           // list of Displayables
Snake *snake = new Snake(GAME_AREA_X + 3 * BLOCKSIZE, GAME_AREA_Y + 3 * BLOCKSIZE);
Fruit fruit;

// all in terms of constants and blocks
GameArea gameArea(GAME_AREA_X, GAME_AREA_Y, BLOCKSIZE * AREA_WIDTH_IN_BLOCKS, BLOCKSIZE * AREA_HEIGHT_IN_BLOCKS);

// game texts
Text txt_score (25, 35, "Score: 0");
Text txt_fps (25, 75, "FPS: 30");
Text txt_speed (25, 575, "Speed: 5");

// pause text
Text txt_pause (350, 330, "PAUSED");

// game over
Text txt_lose (300, 330, "GAME OVER (press r to play again)");

// splash screen texts
Text name (200, 150, "Jin Hao Zhang");
Text userid (200, 200, "jh8zhang");
Text desc_control (200, 300, "Use WSAD keys to control the snake");
Text desc_pause (200, 330, "Press p to pause/unpause the game");
Text desc_res (200, 360, "Press r to restart the level");
Text desc_quit (200, 390, "Press q to quit the game");
Text desc_pressKey (200, 450, "Press r key now to start the game");


void eventLoop(XInfo &xinfo);

/*
 * Initialize X and create a window
 */
void initX(int argc, char *argv[], XInfo &xInfo) {
	XSizeHints hints;
	unsigned long white, black;

	xInfo.display = XOpenDisplay( "" );
	if ( !xInfo.display )	{
		error( "Can't open display." );
	}
	
   /*
	* Find out some things about the display you're using.
	*/
	xInfo.screen = DefaultScreen( xInfo.display );

	white = XWhitePixel( xInfo.display, xInfo.screen );
	black = XBlackPixel( xInfo.display, xInfo.screen );

	hints.x = 100;
	hints.y = 100;
	hints.width = 800;
	hints.height = 600;
	hints.flags = PPosition | PSize;

	xInfo.window = XCreateSimpleWindow( 
		xInfo.display,				// display where window appears
		DefaultRootWindow( xInfo.display ), // window's parent in window tree
		hints.x, hints.y,			// upper left corner location
		hints.width, hints.height,	// size of the window
		Border,						// width of window's border
		black,						// window border colour
		white );					// window background colour
		
	XSetStandardProperties(
		xInfo.display,		// display containing the window
		xInfo.window,		// window whose properties are set
		"Snake",		// window's title
		"Snake",			// icon's title
		None,				// pixmap for the icon
		argv, argc,			// applications command line args
		&hints );			// size hints for the window

	/* 
	 * Create Graphics Contexts
	 */
	int i = 0;
	xInfo.gc[i] = XCreateGC(xInfo.display, xInfo.window, 0, 0);
	XSetForeground(xInfo.display, xInfo.gc[i], BlackPixel(xInfo.display, xInfo.screen));
	XSetBackground(xInfo.display, xInfo.gc[i], WhitePixel(xInfo.display, xInfo.screen));
	XSetFillStyle(xInfo.display, xInfo.gc[i], FillSolid);
	XSetLineAttributes(xInfo.display, xInfo.gc[i],
	                     1, LineSolid, CapButt, JoinRound);

	XSelectInput(xInfo.display, xInfo.window, 
		ButtonPressMask | KeyPressMask | 
		PointerMotionMask | 
		EnterWindowMask | LeaveWindowMask |
		StructureNotifyMask);  // for resize events

	/*
	 * Put the window on the screen.
	 */
	XMapRaised( xInfo.display, xInfo.window );
	XFlush(xInfo.display);
}

void repaint( XInfo &xinfo) {
	list<Displayable *>::const_iterator begin = dList.begin();
	list<Displayable *>::const_iterator end = dList.end();

	XClearWindow(xinfo.display, xinfo.window);
	
	// get height and width of window (might have changed since last repaint)

	XWindowAttributes windowInfo;
	XGetWindowAttributes(xinfo.display, xinfo.window, &windowInfo);
	unsigned int height = windowInfo.height;
	unsigned int width = windowInfo.width;

	// big black rectangle to clear background
    
	// draw display list
	while( begin != end ) {
		Displayable *d = *begin;
		d->paint(xinfo);
		begin++;
	}
	XFlush( xinfo.display );
}

void handleKeyPress(XInfo &xinfo, XEvent &event) {
	KeySym key;
	char text[BufferSize];
	
	int i = XLookupString( 
		(XKeyEvent *)&event, 	// the keyboard event
		text, 					// buffer when text will be written
		BufferSize, 			// size of the text buffer
		&key, 					// workstation-independent key symbol
		NULL );					// pointer to a composeStatus structure (unused)


	if ( i == 1) {
		// handle 
		switch (text[0]){
			case 'q':
				error("Terminating normally.");
				break;
			case 'r':
				if (0 == gameState){
					// exit splash screen
					updateGameState(1);
					repaint(xinfo);
				} else {
					// reset 
					snake = new Snake(GAME_AREA_X + 3 * BLOCKSIZE, GAME_AREA_Y + 3 * BLOCKSIZE);
					snake->setSpeed(speed);
					fruit;
					score = 0;
					txt_score.updateText("Score: " + patch::to_string(score));
					updateGameState(0);
				}
				break;
			case 'p':
				//HANDLE PAUSE
				if (gameState == 1){
					updateGameState (2);
				} else if (gameState == 2){
					updateGameState (1);
				}
				break;
			case 'w':
			case 'a':
			case 's':
			case 'd':
				if (1 == gameState) {
					// SNAKE MOVEMENT
					snake->onUserChangeDirection(text[0]);
				}
				break;
		}
		
	}
}

// updates gameState 
void updateGameState (int state){
	gameState = state;

	// update 
	dList.clear();

	switch (gameState) {
		case 0:
			dList.push_back (&name);
			dList.push_back (&userid);
			dList.push_back (&desc_quit);
			dList.push_back (&desc_res);
			dList.push_back (&desc_pause);
			dList.push_back (&desc_control);
			dList.push_back (&desc_pressKey);
			break;
		case 1: case 2: case 3:
			// txts
			dList.push_back (&txt_speed);
			dList.push_back (&txt_fps);
			dList.push_back (&txt_score);

			// game objects
			dList.push_back (snake);
			dList.push_back (&fruit);

			// game area
			dList.push_back (&gameArea);

			if (2 == gameState){
				dList.push_front (&txt_pause); // paused
			} else if (3 == gameState){
				dList.push_front (&txt_lose);
			}
			break;
	}
}

// get microseconds
unsigned long now() {
	timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec * 1000000 + tv.tv_usec;
}

// checks to see if snake is out of area or ate itself
void checkGameOverConditions (GameArea ga, Snake  *snake) {
	if (!ga.isInArea(snake->getX(), snake->getY()) || snake->ateSelf()){
		updateGameState(3); // lose
	}
}

// see if the fruit got eaten
bool checkAndHandleFruit (XInfo &xinfo){
	if (snake->getY() == fruit.getY() && snake->getX() == fruit.getX()){
		
		score++;
		txt_score.updateText("Score: " + patch::to_string(score));
		
		snake->grow(xinfo);
		fruit.newPosition(snake);

		return true;
	}

	return false;
}

// checks if there should be a repaint
bool needToRepaint(XInfo &xinfo, int inside) {
	if (gameState == 1){
		bool needToRepaint = snake->move(xinfo);
		
		// check and handle collisons
		checkGameOverConditions(gameArea, snake);

		return needToRepaint;
	} else {
		return true;
	}
}

// game loop
void eventLoop(XInfo &xinfo) {	
	XEvent event;
	unsigned long lastRepaint = 0;
	int inside = 0;

	while( true ) {
		if (XPending(xinfo.display) > 0) {
			XNextEvent( xinfo.display, &event );
			switch( event.type ) {
				case KeyPress:
					handleKeyPress(xinfo, event);
					break;
				case EnterNotify:
					inside = 1;
					break;
				case LeaveNotify:
					inside = 0;
					break;
			}
		} 

		usleep(1000000/FPS);
		if (needToRepaint(xinfo, inside)) {
			checkAndHandleFruit(xinfo); // only check if repainted
			repaint(xinfo);
		}
	}
}

int main (int argc, char *argv[]) {
	XInfo xInfo;
	initX(argc, argv, xInfo);

	// default values
	FPS = 30;
	speed = 5;

	if (argc == 3) {
		// FPS can be 1 -100, speed 1 - 10
		if (atoi(argv[1]) <= 100 && atoi(argv[1]) >= 1 && atoi(argv[2]) >= 1 && atoi(argv[2]) <= 10){
			FPS = atoi(argv[1]);
			speed = atoi(argv[2]);

			cout << "user set custom FPS and speed" << endl;
		} else {
			cout << "invalid FPS and speed params" << endl;
		}

	} else {
		cout << "no params" << endl;
	}

	txt_speed.updateText("Speed: " + patch::to_string(speed));
	txt_fps.updateText("FPS: " + patch::to_string(FPS));

	snake->setSpeed(speed);
	
	// change font
	XFontStruct * font;
	font = XLoadQueryFont (xInfo.display, "12x24");
	XSetFont (xInfo.display, xInfo.gc[0], font->fid);

	// get bmp file
	int res = XReadBitmapFile(xInfo.display, xInfo.window, "apple.XBM", &fruit_w, &fruit_h, &bmp_fruit, NULL, NULL);
	
	updateGameState(0); // do this everytime gamestate is changed

	// game looooooooooooooooop
	eventLoop(xInfo);

	XCloseDisplay(xInfo.display);

	XFreePixmap(xInfo.display, bmp_fruit);
	delete (snake);
}